import chai, { should } from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import asyncWrapper from '../../src/middleware/asyncWrapper';

chai.use(sinonChai);

should();

describe('Async validator', () => {
  const request = {};

  it('should catch all errors', () => {
    const response = { status() {}, json() {} };
    const wrapFunction = sinon.stub().throws();
    const next = sinon.spy();
    const wrappedFunction = asyncWrapper(wrapFunction);

    wrappedFunction(request, response, next);
    next.should.be.calledOnce;
  });
});
