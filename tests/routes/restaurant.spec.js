import request from 'supertest';
import chai, { should, expect } from 'chai';
import jwt from 'jsonwebtoken';
import { config } from 'dotenv';

import app from '../../src/app';
import Models from '../../src/models';
import {
  address,
  vendor,
  vendorTwo,
  addressUpdate,
} from '../fixtures/vendors.mock';

config();
const { Restaurant, Address } = Models;

const setupDB = async () => {
  await Restaurant.create(vendor);
  await Restaurant.create(vendorTwo);
};

const teardownDB = async () => {
  await Restaurant.destroy({
    where: {},
  });
};

const tokenOne = jwt.sign({ id: vendor.id }, process.env.JWT_KEY);
const tokenTwo = jwt.sign({ id: vendorTwo.id }, process.env.JWT_KEY);

describe.skip('restaurant address', () => {});

describe.skip('create restaurant address', () => {
  before(() => {
    setupDB();
  });

  after(() => {
    teardownDB();
  });

  it('should not create address for unauthenticated users', async () => {
    const response = await request(app)
      .post('/vendors/03b02e14-e3e4-4608-b6a7-d6f96f44f083/address')
      .send(address);
    response.status.should.equal(401);
    response.body.status.should.equal('error');
  });

  it('should create address for authenticated users', async () => {
    const response = await request(app)
      .post(`/vendors/${vendor.id}/address`)
      .set('Authorization', `Bearer ${tokenOne}`)
      .send(address);

    const dbAddress = await Address.findOne({
      where: {
        restaurantId: vendor.id,
      },
    });

    expect(dbAddress).to.not.be.null;
    expect(dbAddress.location).to.eql(address.location);
    response.status.should.equal(201);
    response.body.status.should.equal('success');
    expect(response.body.data).to.haveOwnProperty('address');
    expect(response.request._header).to.haveOwnProperty('authorization');
    expect(response.request._header.authorization).to.include('Bearer ');
    expect(response.body.data.address.restaurantId).to.eql(vendor.id);
  });

  it('should allow users create address for only their own restaurants', async () => {
    const response = await request(app)
      .post(`/vendors/${vendor.id}/address`)
      .set('Authorization', `Bearer ${tokenTwo}`)
      .send(address);

    response.status.should.equal(401);
    expect(response.body.error.message).to.eql('You are not permitted perform this operation');
  });

  it('should throw an error if restaurant is not found', async () => {
    const response = await request(app)
      .post('/vendors/5f77c209-68f6-4c08-a133-798a5f90483c/address')
      .set('Authorization', `Bearer ${tokenOne}`)
      .send(address);

    response.status.should.equal(404);
    expect(response.body.error.message).to.eql('Restaurant not found');
  });

  it('should fetch a restaurant address', async () => {
    const response = await request(app)
      .get(`/vendors/${vendor.id}/address`);

    response.status.should.equal(200);
    expect(response.body.data).to.haveOwnProperty('address');
  });

  it('should return error if address is not found', async () => {
    const response = await request(app)
      .get(`/vendors/${vendorTwo.id}/address`);

    response.status.should.equal(404);
  });

  it('should not update a restaurant address if request is not made by owner', async () => {
    const response = await request(app)
      .patch(`/vendors/${vendor.id}/address`)
      .set('Authorization', `Bearer ${tokenTwo}`)
      .send(addressUpdate);

    const dbAddress = await Address.findOne({
      where: {
        restaurantId: vendor.id,
      },
    });

    response.status.should.equal(401);
    response.body.error.message.should.equal('You are not permitted perfom this operation');
    dbAddress.location.should.equal(address.location);
  });

  it('should update a restaurant address if request is made by owner', async () => {
    const response = await request(app)
      .patch(`/vendors/${vendor.id}/address`)
      .set('Authorization', `Bearer ${tokenOne}`)
      .send(addressUpdate);

    const dbAddress = await Address.findOne({ where: { restaurantId: vendor.id } });

    response.status.should.equal(200);
    response.body.data.address.location.should.equal('27, Amos Street');
    dbAddress.location.should.equal('27, Amos Street');
  });

  it('should return an error if an update request is made for a restaurant with no address', async () => {
    const response = await request(app)
      .patch(`/vendors/${vendorTwo.id}/address`)
      .set('Authorization', `Bearer ${tokenTwo}`)
      .send(addressUpdate);

    response.status.should.equal(404);
  });

  it('should not delete restaurant address if request not is made by owner', async () => {
    const response = await request(app)
      .delete(`/vendors/${vendor.id}/address`)
      .set('Authorization', `Bearer ${tokenTwo}`);

    const dbAddress = await Address.findOne({ where: { restaurantId: vendor.id } });

    expect(dbAddress).to.not.be.null;
    response.status.should.equal(401);
  });

  it('should delete restaurant address if request is made by owner', async () => {
    const response = await request(app)
      .delete(`/vendors/${vendor.id}/address`)
      .set('Authorization', `Bearer ${tokenOne}`);

    const dbAddress = await Address.findOne({ where: { restaurantId: vendor.id } });

    expect(dbAddress).to.be.null;
    response.status.should.equal(204);
  });

  it('should return an error if an delete request is made for a restaurant with no address', async () => {
    const response = await request(app)
      .delete(`/vendors/${vendorTwo.id}/address`)
      .set('Authorization', `Bearer ${tokenTwo}`);

    response.status.should.equal(404);
  });
});
