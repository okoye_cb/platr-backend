import axios from 'axios';
import chai, { should } from 'chai';
import jwt from 'jsonwebtoken';
import { OAuth2Client } from 'google-auth-library';
import request from 'supertest';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

import app from '../../src/app';
import Model from '../../src/models';
import {
  customer,
  facebookFirstCallResponse,
  facebookSecondCallErrorResponse,
  facebookSecondCallInvalidToken,
  facebookSecondCallSuccessResponse,
  facebookThirdCallSuccessResponse,
  googleSuccessResponse,
  resetSampleUser,
  restaurant,
  restaurantDuplicate,
} from '../fixtures/auth.mock';

chai.use(sinonChai);
should();

const { Customer, Restaurant } = Model;

describe('Authentication Routes for customers', () => {
  describe('Customer signup route', () => {
    it('should signup successfully', async () => {
      const response = await request(app)
        .post('/auth/customers/signup')
        .send(customer);

      response.status.should.equal(201);
      response.body.status.should.eql('success');
      response.body.should.have.property('data');
      response.body.data.should.have.property('token');
      response.body.data.token.should.be.a('string');
      response.body.data.should.have.property('user');
      response.body.data.user.should.have.property('id');
      response.body.data.user.should.have.property('firstName');
      response.body.data.user.should.have.property('lastName');
      response.body.data.user.should.have.property('email');
      response.body.data.user.should.have.property('phone');
    });

    specify('error if email is already registered', async () => {
      const response = await request(app)
        .post('/auth/customers/signup')
        .send(customer);

      response.status.should.equal(409);
      response.body.status.should.eql('error');
      response.body.should.have.property('error');
      response.body.error.should.have.property('message');
    });
  });

  describe('Customer signin route', () => {
    after(async () => {
      await Customer.destroy({ where: {} });
    });

    it('should signin successfully', async () => {
      const response = await request(app)
        .post('/auth/customers/signin')
        .send({
          email: customer.email,
          password: customer.password,
        });

      response.status.should.equal(200);
      response.body.status.should.eql('success');
      response.body.should.have.property('data');
      response.body.data.should.have.property('token');
      response.body.data.token.should.be.a('string');
      response.body.data.should.have.property('userDetails');
    });

    specify('error for wrong email', async () => {
      const response = await request(app)
        .post('/auth/customers/signin')
        .send({
          email: 'wrongemail@gmail.com',
          password: restaurant.password,
        });

      response.status.should.equal(401);
      response.body.status.should.eql('error');
      response.body.error.message.should.eql('email or password is incorrect');
    });

    specify('error for wrong password', async () => {
      const response = await request(app)
        .post('/auth/customers/signin')
        .send({
          email: restaurant.email,
          password: 'wrongpassword',
        });

      response.status.should.equal(401);
      response.body.status.should.eql('error');
      response.body.error.message.should.eql('email or password is incorrect');
    });
  });
});

describe('Authentication Routes for vendors', () => {
  describe('Restaurant signup route', () => {
    it('should signup successfully', async () => {
      const response = await request(app)
        .post('/auth/vendors/signup')
        .send(restaurant);

      response.status.should.equal(201);
      response.body.status.should.eql('success');
      response.body.should.have.property('data');
      response.body.data.should.have.property('token');
      response.body.data.token.should.be.a('string');
      response.body.data.should.have.property('restaurant');
      response.body.data.restaurant.should.have.property('id');
      response.body.data.restaurant.should.have.property('ownerName');
      response.body.data.restaurant.should.have.property('userName');
      response.body.data.restaurant.should.have.property('businessName');
      response.body.data.restaurant.should.have.property('email');
      response.body.data.restaurant.should.have.property('securityQuestion');
      response.body.data.restaurant.should.have.property('securityAnswer');
    });

    specify('error if username/email/businessName is already registered', async () => {
      const response = await request(app)
        .post('/auth/vendors/signup')
        .send(restaurantDuplicate);

      response.status.should.equal(409);
      response.body.status.should.eql('error');
      response.body.should.have.property('error');
      response.body.error.should.have.property('message');
    });
  });

  describe('Restaurant signup route request validation', () => {
    specify('error if ownerName is not supplied (undefined)', async () => {
      const restaurantRequest = {
        userName: restaurant.userName,
        businessName: restaurant.businessName,
        email: restaurant.email,
        password: restaurant.password,
        securityQuestion: restaurant.securityQuestion,
        securityAnswer: restaurant.securityAnswer,
      };
      const response = await request(app)
        .post('/auth/vendors/signup')
        .send(restaurantRequest);

      response.status.should.equal(400);
      response.body.status.should.eql('error');
      response.body.should.have.property('error');
      response.body.error.errors
        .ownerName.should.eql('Provide your fullname to continue');
    });

    specify('error if userName is not supplied (undefined)', async () => {
      const restaurantRequest = {
        ownerName: restaurant.ownerName,
        businessName: restaurant.businessName,
        email: restaurant.email,
        password: restaurant.password,
        securityQuestion: restaurant.securityQuestion,
        securityAnswer: restaurant.securityAnswer,
      };
      const response = await request(app)
        .post('/auth/vendors/signup')
        .send(restaurantRequest);

      response.status.should.equal(400);
      response.body.status.should.eql('error');
      response.body.should.have.property('error');
      response.body.error.errors
        .userName.should.eql('Provide a username to continue');
    });

    specify('error if businessName is not supplied (undefined)', async () => {
      const restaurantRequest = {
        ownerName: restaurant.ownerName,
        userName: restaurant.userName,
        email: restaurant.email,
        password: restaurant.password,
        securityQuestion: restaurant.securityQuestion,
        securityAnswer: restaurant.securityAnswer,
      };
      const response = await request(app)
        .post('/auth/vendors/signup')
        .send(restaurantRequest);

      response.status.should.equal(400);
      response.body.status.should.eql('error');
      response.body.should.have.property('error');
      response.body.error.errors
        .businessName.should.eql('Provide the business name for your restaurant');
    });

    specify('error if email is not supplied (undefined)', async () => {
      const restaurantRequest = {
        ownerName: restaurant.ownerName,
        userName: restaurant.userName,
        businessName: restaurant.businessName,
        password: restaurant.password,
        securityQuestion: restaurant.securityQuestion,
        securityAnswer: restaurant.securityAnswer,
      };
      const response = await request(app)
        .post('/auth/vendors/signup')
        .send(restaurantRequest);

      response.status.should.equal(400);
      response.body.status.should.eql('error');
      response.body.should.have.property('error');
      response.body.error.errors
        .email.should.eql('Email address is required');
    });

    specify('error if email provided is invalid', async () => {
      const restaurantRequest = {
        ownerName: restaurant.ownerName,
        userName: restaurant.userName,
        businessName: restaurant.businessName,
        email: 'info.com',
        password: restaurant.password,
        securityQuestion: restaurant.securityQuestion,
        securityAnswer: restaurant.securityAnswer,
      };
      const response = await request(app)
        .post('/auth/vendors/signup')
        .send(restaurantRequest);

      response.status.should.equal(400);
      response.body.status.should.eql('error');
      response.body.should.have.property('error');
      response.body.error.errors
        .email.should.eql('Enter a valid email address');
    });

    specify('error if password is not provided', async () => {
      const restaurantRequest = {
        ownerName: restaurant.ownerName,
        userName: restaurant.userName,
        businessName: restaurant.businessName,
        email: restaurant.email,
        securityQuestion: restaurant.securityQuestion,
        securityAnswer: restaurant.securityAnswer,
      };
      const response = await request(app)
        .post('/auth/vendors/signup')
        .send(restaurantRequest);

      response.status.should.equal(400);
      response.body.status.should.eql('error');
      response.body.should.have.property('error');
      response.body.error.errors
        .password.should.eql('Password cannot be blank');
    });

    specify('error if password provided is less than minimum length', async () => {
      const restaurantRequest = {
        ownerName: restaurant.ownerName,
        userName: restaurant.userName,
        businessName: restaurant.businessName,
        email: restaurant.email,
        password: 'short',
        securityQuestion: restaurant.securityQuestion,
        securityAnswer: restaurant.securityAnswer,
      };
      const response = await request(app)
        .post('/auth/vendors/signup')
        .send(restaurantRequest);

      response.status.should.equal(400);
      response.body.status.should.eql('error');
      response.body.should.have.property('error');
      response.body.error.errors
        .password.should.eql('Password should not be less than 8 characters');
    });

    specify('error if security question is not provided', async () => {
      const restaurantRequest = {
        ownerName: restaurant.ownerName,
        userName: restaurant.userName,
        businessName: restaurant.businessName,
        email: restaurant.email,
        password: restaurant.password,
        securityAnswer: restaurant.securityAnswer,
      };
      const response = await request(app)
        .post('/auth/vendors/signup')
        .send(restaurantRequest);

      response.status.should.equal(400);
      response.body.status.should.eql('error');
      response.body.should.have.property('error');
      response.body.error.errors
        .securityQuestion.should.eql('Provide your security question');
    });

    specify('error if security answer is not provided', async () => {
      const restaurantRequest = {
        ownerName: restaurant.ownerName,
        userName: restaurant.userName,
        businessName: restaurant.businessName,
        email: restaurant.email,
        password: restaurant.password,
        securityQuestion: restaurant.securityQuestion,
      };
      const response = await request(app)
        .post('/auth/vendors/signup')
        .send(restaurantRequest);

      response.status.should.equal(400);
      response.body.status.should.eql('error');
      response.body.should.have.property('error');
      response.body.error.errors
        .securityAnswer.should.eql('Provide the answer to your security question');
    });
  });

  describe('Restaurant signin route', () => {
    after(async () => {
      await Restaurant.destroy({ where: {} });
    });

    it('should signin restaurant owners successfully', async () => {
      const response = await request(app)
        .post('/auth/vendors/signin')
        .send({
          email: restaurant.email,
          password: restaurant.password,
        });

      response.status.should.equal(200);
      response.body.status.should.eql('success');
      response.body.should.have.property('data');
      response.body.data.should.have.property('token');
      response.body.data.token.should.be.a('string');
      response.body.data.should.have.property('restaurant');
    });

    specify('error for wrong email', async () => {
      const response = await request(app)
        .post('/auth/vendors/signin')
        .send({
          email: 'wrongemail@gmail.com',
          password: restaurant.password,
        });

      response.status.should.equal(401);
      response.body.status.should.eql('error');
      response.body.error.message.should.eql('email or password is incorrect');
    });

    specify('error for wrong password', async () => {
      const response = await request(app)
        .post('/auth/vendors/signin')
        .send({
          email: restaurant.email,
          password: 'wrongpassword',
        });

      response.status.should.equal(401);
      response.body.status.should.eql('error');
      response.body.error.message.should.eql('email or password is incorrect');
    });
  });

  describe('Restaurant signin route request validation', () => {
    specify('error if email is not supplied (undefined)', async () => {
      const restaurantRequest = {
        password: restaurant.password,
      };
      const response = await request(app)
        .post('/auth/vendors/signin')
        .send(restaurantRequest);

      response.status.should.equal(400);
      response.body.status.should.eql('error');
      response.body.should.have.property('error');
      response.body.error.errors
        .email.should.eql('Email address is required');
    });

    specify('error if email provided is invalid', async () => {
      const restaurantRequest = {
        email: 'info.com',
        password: restaurant.password,
      };
      const response = await request(app)
        .post('/auth/vendors/signin')
        .send(restaurantRequest);

      response.status.should.equal(400);
      response.body.status.should.eql('error');
      response.body.should.have.property('error');
      response.body.error.errors
        .email.should.eql('Enter a valid email address');
    });

    specify('error if password is not provided', async () => {
      const restaurantRequest = {
        email: restaurant.email,
      };
      const response = await request(app)
        .post('/auth/vendors/signin')
        .send(restaurantRequest);

      response.status.should.equal(400);
      response.body.status.should.eql('error');
      response.body.should.have.property('error');
      response.body.error.errors
        .password.should.eql('Password cannot be blank');
    });

    specify('error if password provided is less than minimum length', async () => {
      const restaurantRequest = {
        email: restaurant.email,
        password: 'short',
      };
      const response = await request(app)
        .post('/auth/vendors/signin')
        .send(restaurantRequest);

      response.status.should.equal(400);
      response.body.status.should.eql('error');
      response.body.should.have.property('error');
      response.body.error.errors
        .password.should.eql('Password should not be less than 8 characters');
    });
  });
});

describe('Social authentication routes', () => {
  describe('Google Authentication Method', () => {
    let googleStub;

    beforeEach(() => {
      googleStub = sinon.stub(OAuth2Client.prototype, 'verifyIdToken').returns({
        getPayload: () => googleSuccessResponse,
      });
    });

    afterEach(() => OAuth2Client.prototype.verifyIdToken.restore());

    it('should create a new user', async () => {
      const response = await request(app)
        .post('/auth/google')
        .send({ accessToken: '' });

      googleStub.should.be.calledOnce;
      response.status.should.equal(201);
      response.body.status.should.eql('success');
      response.body.data.user.should.be.an('object');
    });
  });

  describe('Facebook Authentication Method', () => {
    let facebook;
    let sampleToken;

    beforeEach(() => {
      facebook = sinon.stub(axios, 'get');
      facebook.onFirstCall().returns({ data: '' });
      sampleToken = { accessToken: 'Invalid access token' };
    });

    afterEach(() => axios.get.restore());

    specify('error if access token is invalid', async () => {
      facebook.onSecondCall().returns({
        data: { data: facebookSecondCallErrorResponse },
      });

      const response = await request(app)
        .post('/auth/facebook')
        .send(sampleToken);

      facebook.should.be.calledTwice;
      response.status.should.equal(500);
      response.body.status.should.eql('error');
    });

    specify('error if facebook access token is invalid', async () => {
      facebook.onSecondCall().returns({
        data: { data: facebookSecondCallInvalidToken },
      });

      const response = await request(app)
        .post('/auth/facebook')
        .send(sampleToken);

      facebook.should.be.calledTwice;
      response.status.should.equal(500);
      response.body.status.should.eql('error');
    });

    it.skip('should create a new user', async () => {
      facebook.onSecondCall().returns({
        data: { data: facebookSecondCallSuccessResponse },
      });
      facebook.onThirdCall().returns({ data: facebookThirdCallSuccessResponse });

      const response = await request(app)
        .post('/auth/facebook')
        .send(sampleToken);

      facebook.should.be.calledThrice;
      response.status.should.equal(201);
      response.body.status.should.eql('success');
      response.body.data.user.should.be.an('object');
    });
  });
});

describe.skip('Password reset and update routes', () => {
  let validToken;
  let validId;
  let secret;
  const tokens = {};

  before(async () => {
    const user = await request(app)
      .post('/auth/signup')
      .send(resetSampleUser);
    const userObject = await Customer.findByPk(user.body.data.customer.id);

    secret = `${userObject.password}!${userObject.createdAt.toISOString()}`;
    tokens.validToken = jwt.sign({ id: userObject.id }, secret);
    tokens.invalidToken = jwt.sign({ id: userObject.id }, 'secret');
    validToken = user.body.data.token;
    validId = user.body.data.user.id;
  });

  describe('password reset', () => {
    specify('statusCode 404 for non-existing user', async () => {
      const response = await request(app)
        .post('/auth/reset_password')
        .send({ email: 'notfound@email.com' });

      response.status.should.equal(404);
      response.body.should.be.an('object');
      response.body.should.have.property('error');
    });

    it('should return 200 for existing user', async () => {
      const response = request(app)
        .post('/auth/reset_password')
        .send({ email: resetSampleUser.email });

      response.status.should.equal(200);
      response.body.should.have.property('data');
    });
  });

  describe('password update', () => {
    it('should return 200 for existing user', async () => {
      const response = await request(app)
        .post('/auth/reset_password')
        .send({ email: resetSampleUser.email });

      response.status.should.equal(200);
      response.body.should.have.property('data');
    });

    specify('error if user is found, token is valid, but password is empty', async () => {
      const response = await request(app)
        .patch(`/auth/update_password/${validId}/${tokens.validToken}`)
        .send({ password: '' });

      response.status.should.equal(400);
      response.body.should.be.an('object');
      response.body.should.have.property('error');
    });

    it('should return success if user, password and token are valid', async () => {
      const response = await request(app)
        .patch(`/auth/update_password/${validId}/${tokens.validToken}`)
        .send({ password: 'newPassword' });

      response.status.should.equal(200);
      response.body.should.be.an('object');
      response.body.message.should.eql('password updated successfully');
    });
  });
});
