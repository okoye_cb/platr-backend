import { should as importedShould } from 'chai';
import sinon from 'sinon';
import faker from 'faker';
import sendgrid from '@sendgrid/mail';

import notification from '../../src/services/notification';
import Models from '../../src/models';

const { Users, Followers, Articles } = Models;

const should = importedShould();

/*
describe('Notification System', () => {
  before(async () => {
    sinon.stub(sendgrid, 'send').resolves();
    articles[0].authorId = usersWithFollowing[0].followeeId;
    await Articles.destroy({ where: {} });
    await Users.bulkCreate(users);
    await Followers.bulkCreate(usersWithFollowing);
    await Articles.bulkCreate(articles);
  });

  after(async () => {
    if (sendgrid.send.restore) sendgrid.send.restore();
    await Articles.destroy({ where: {} });
  });

  describe('Notifications', () => {
    it('should return undefined for unsupported types', () => {
      const payload = { type: 'unsupported', email: 'demo@demo.com', userId: 'id' };
      const data = notification.handleNotification(payload);

      should.equal(data, undefined);
    });

    it('should handle notification based on type', () => {
      const payload = {
        type: 'accountActivation',
        payload: { email: 'demo@demo.com', userId: 'id' }
      };
      const data = notification.handleNotification(payload);

      should.not.equal(data, undefined);
    });

    it('should add custom notification type with handler if not existent', () => {
      const notify = notification.addNotification('testFollowing', sinon.spy());

      notify.should.be.eql(notification);
    });

    it('should return notification instance with existent notification type', () => {
      const notify = notification.addNotification('testFollowing', sinon.spy());

      notify.should.be.eql(notification);
    });

    it('should handle notification based on type', () => {
      const handler = sinon.spy();
      const notificationType = 'userFollowing';
      const notificationPayload = { email: 'demo@demo.com', userId: 'id' };
      const payload = { type: notificationType, payload: notificationPayload };

      notification.addNotification(notificationType, handler);
      notification.handleNotification(payload);

      handler.should.be.calledOnce;
      handler.should.be.calledWith(notificationPayload);
    });
  });
});

describe('Password recovery Notification', () => {
  it('should handle notification for password recovery', () => {
    const payload = { type: 'passwordRecovery', payload: { email: 'demo@demo.com', userId: 'id' } };
    const data = notification.handleNotification(payload);
    should.not.equal(data, undefined);
  });
});
*/
