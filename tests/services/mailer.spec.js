/* eslint import/no-unresolved: 0 */
import chai, { should } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import faker from 'faker';
import sendgrid from '@sendgrid/mail';
import sinon from 'sinon';
import sinonTest from 'sinon-test';

import mailer from '../../src/services/mailer';

chai.use(chaiAsPromised);
should();

const test = sinonTest(sinon);

describe('Email service', () => {
  const emailData = {
    to: faker.internet.email(),
    from: faker.internet.email(),
    templateId: faker.random.uuid(),
    dynamic_template_data: {
      name: faker.name.findName(),
    },
  };

  afterEach(() => {
    if (sendgrid.send.restore) sendgrid.send.restore();
  });

  specify('an error if template is not available', async () => {
    const data = await mailer({ type: 'unavailable', payload: emailData });
    data.should.be.instanceOf(Error);
  });

  it('should send message with array of mail data', test(async () => {
    sinon.stub(sendgrid, 'send').resolves();

    const response = await mailer({
      type: 'accountActivation',
      payload: [emailData],
    });

    response.should.have.property('message');
  }));

  specify('an error if mail data is undefined', test(async () => {
    const data = await mailer({ type: 'accountActivation' });
    data.should.be.instanceOf(Error);
  }));

  it('should catch sendgrid error', test(async () => {
    sinon.stub(sendgrid, 'send').rejects();

    (mailer({ type: 'accountActivation', payload: emailData }))
      .should.eventually.fulfilled;
  }));
});
