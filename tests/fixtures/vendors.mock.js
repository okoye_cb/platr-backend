export const vendor = {
  id: 'cad43ae9-be0e-46c9-9a19-e670526b5dab',
  ownerName: 'lata Adigbun',
  password: '123456789',
  userName: 'adigunP',
  businessName: 'Adigun Courier Foods',
  email: 'plattra@foods.com',
  securityQuestion: 'What is your favorite movie',
  securityAnswer: 'The Irishman',
};

export const vendorTwo = {
  password: '123456789',
  id: '5f77c209-68f6-4c08-a133-798a5f90483b',
  ownerName: 'Ajala Jalingo',
  userName: 'ajala',
  businessName: 'Ajala Travel Foods',
  email: 'jalajalingo@foods.com',
  securityQuestion: 'What is your favorite movie',
  securityAnswer: 'The Irishman',
};

export const addressUpdate = {
  region: 'kaduna north',
  location: '27, Amos Street',
};

export const address = {
  state: 'kaduna',
  region: 'kaduna south',
  location: '27, Adamu Street',
};
