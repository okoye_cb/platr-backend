# Platr-Backend

Platr is an online delivery system that serves as an online service that brings restaurants, food vendors and the final consumers of the food together.

## Setup Instructions

### Prerequisites

Ensure you have the following installed on your local machine:

- [NodeJS](https://nodejs.org/en/download/)
- [PostgreSQL](https://www.postgresql.org/download/)

### Installing/Running locally

- Clone or fork repo🤷‍♂

  ```bash
    - git clone <repo>
    - cd Platr-Backend
    - npm install
  ```

- Create a PostgreSQL database by running the command below in `psql`

  ```bash
    createdb -h localhost -p 5432 -U postgres <database-for-dev>
  ```

- Create/configure `.env` environment with your credentials. A sample `.env.example` file has been provided to get you started. Make a duplicate of `.env.example` and rename to `.env`, then configure your credentials (ensure to provide the correct details).

- Run `npm run dev` to start the server and watch for changes
