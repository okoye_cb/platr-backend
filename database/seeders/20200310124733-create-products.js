import { v4 as uuidv4 } from 'uuid';

export default {
  up: async (queryInterface, Sequelize) => {
    const restaurantId = await queryInterface.rawSelect('Restaurants', {}, ['id']);
    const categoryId = await queryInterface.rawSelect('Categories', {}, ['id']);

    if (!(restaurantId && categoryId)) {
      throw new Error("Invalid 'vendorId' or 'categoryId'");
    }

    return queryInterface.bulkInsert('Products', [
      {
        id: uuidv4(),
        name: 'Soup - Knorr, Chicken Gumbo',
        minOrderPrice: 350.00,
        description: 'Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.',
        imageURL: 'http://dummyimage.com/237x164.jpg/dddddd/000000',
        restaurantId,
        categoryId,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('Products', {}),
};
