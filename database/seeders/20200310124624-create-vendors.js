import bcrypt from 'bcryptjs';
import { v4 as uuidv4 } from 'uuid';

require('dotenv').config();

const salt = +process.env.SALT;

export default {
  up: async (queryInterface, Sequelize) => queryInterface.bulkInsert('Restaurants', [
    {
      id: uuidv4(),
      businessName: 'Better Chao',
      about: 'Serving you the best of continental dishes',
      avatar: 'https://placeholder.com/150',
      email: 'tundeajagbea@gmail.com',
      password: await bcrypt.hash('123456789', await bcrypt.genSalt(salt)),
      status: 'active',
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id: uuidv4(),
      businessName: 'Divape',
      about: 'Serving you the best of inter-continental dishes',
      email: 'cvondra0@reverbnation.com',
      avatar: 'https://placeholder.com/150',
      password: await bcrypt.hash('fZPatsGaQ30', await bcrypt.genSalt(salt)),
      status: 'pending',
      createdAt: new Date(),
      updatedAt: new Date(),
    },
  ], {}),

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete(
    'Restaurants',
    null,
    {},
  ),
};
