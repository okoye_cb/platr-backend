import { DataTypes } from 'sequelize';

export default {
  up: (queryInterface, Sequelize) => queryInterface.createTable('Orders', {
    id: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: Sequelize.literal('uuid_generate_v4()'),
    },
    customerId: {
      type: Sequelize.UUID,
      references: {
        model: 'Customers',
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    customerName: {
      type: Sequelize.STRING,
    },
    customerEmail: {
      type: Sequelize.STRING,
    },
    customerPhone: {
      type: Sequelize.STRING,
    },
    deliveryAddress: {
      type: Sequelize.STRING,
    },
    restaurantId: {
      allowNull: false,
      type: Sequelize.UUID,
      references: {
        model: 'Restaurants',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    deliveryFee: {
      type: Sequelize.FLOAT,
    },
    totalPrice: {
      type: Sequelize.FLOAT,
    },
    totalQuantity: {
      type: Sequelize.INTEGER,
    },
    paymentMethod: {
      type: Sequelize.STRING,
      default: 'card',
    },
    status: {
      type: Sequelize.ENUM('cancelled', 'in-progress', 'ready', 'delivered'),
    },
    paymentStatus: {
      type: Sequelize.ENUM('successful', 'failed'),
    },
    txnReference: {
      type: DataTypes.STRING,
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    },
  }),

  down: (queryInterface, Sequelize) => queryInterface.dropTable('Orders'),
};
