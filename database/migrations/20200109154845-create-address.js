export default {
  up: async (queryInterface, Sequelize) => {
    queryInterface.createTable('Addresses', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.literal('uuid_generate_v4()'),
      },
      state: {
        type: Sequelize.STRING,
      },
      region: {
        type: Sequelize.STRING,
      },
      location: {
        type: Sequelize.STRING,
      },
      customerId: {
        allowNull: true,
        type: Sequelize.UUID,
        references: {
          model: 'Customers',
          key: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      },
      restaurantId: {
        allowNull: true,
        type: Sequelize.UUID,
        references: {
          model: 'Restaurants',
          key: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    });
  },

  down: (queryInterface, Sequelize) => queryInterface.dropTable('Addresses'),
};
