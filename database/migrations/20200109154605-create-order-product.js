export default {
  up: (queryInterface, Sequelize) => queryInterface.createTable('OrderProducts', {
    productId: {
      type: Sequelize.UUID,
      primaryKey: true,
      references: {
        model: 'Products',
        key: 'id',
      },
      allowNull: false,
      onUpdate: 'CASCADE',
      onDelete: 'NO ACTION',
    },
    orderId: {
      type: Sequelize.UUID,
      primaryKey: true,
      references: {
        model: 'Orders',
        key: 'id',
      },
      allowNull: false,
      onUpdate: 'CASCADE',
      onDelete: 'NO ACTION',
    },
    quantity: {
      type: Sequelize.FLOAT,
      allowNull: false,
    },
    subtotal: {
      type: Sequelize.FLOAT,
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    },
  }),

  down: (queryInterface, Sequelize) => queryInterface.dropTable('OrderProducts'),
};
