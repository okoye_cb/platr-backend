import { check } from 'express-validator';

export default {
  productSchema: [
    check('name')
      .not()
      .isEmpty()
      .withMessage('name is required')
      .trim()
      .isLength({ min: 2, max: 30 })
      .withMessage('name should be between 2 to 30 characters')
      .customSanitizer((name) => name.toLowerCase()),

    check('minOrderPrice')
      .not()
      .isEmpty()
      .withMessage('Please provide a valid amount'),

    check('description')
      .not()
      .isEmpty()
      .withMessage('Provide a description for the product'),

    check('imageURL').optional(),
  ],

  productUpdateSchema: [
    check('name')
      .optional()
      .trim()
      .isLength({ min: 2, max: 30 })
      .withMessage('name should be between 2 to 30 characters')
      .customSanitizer((name) => name.toLowerCase()),

    check('minOrderPrice')
      .optional()
      .trim(),

    check('description')
      .optional()
      .trim(),

    check('imageURL')
      .optional(),
  ],
};
