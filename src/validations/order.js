import { check } from 'express-validator';

export default {
  orderCreationSchema: [
    check('deliveryFee')
      .not()
      .isEmpty()
      .withMessage('deliveryFee is required')
      .isNumeric()
      .withMessage('deliveryFee must be numeric'),

    check('products')
      .isArray({ min: 1 })
      .withMessage('please add at least one product to your order'),

    check('deliveryAddress')
      .not()
      .isEmpty()
      .withMessage('deliveryAddress field is required')
      .trim()
      .customSanitizer((deliveryAddress) => deliveryAddress.toLowerCase()),
  ],

  anonymousOrderCreationSchema: [
    check('deliveryFee')
      .not()
      .isEmpty()
      .withMessage('deliveryFee is required')
      .isNumeric()
      .withMessage('deliveryFee must be numeric'),

    check('products')
      .isArray({ min: 1 })
      .withMessage('please add at least one product to your order'),

    check('customerEmail')
      .not()
      .isEmpty()
      .withMessage('customerEmail is required')
      .isEmail()
      .withMessage('customerEmail must be a valid email address')
      .trim()
      .customSanitizer((customerEmail) => customerEmail.toLowerCase()),

    check('customerName')
      .not()
      .isEmpty()
      .withMessage('customerName is required'),

    check('customerPhone')
      .not()
      .isEmpty()
      .withMessage('customerPhone field is required')
      .trim()
      .customSanitizer((customerPhone) => customerPhone.toLowerCase()),

    check('deliveryAddress')
      .not()
      .isEmpty()
      .withMessage('deliveryAddress field is required')
      .trim()
      .customSanitizer((deliveryAddress) => deliveryAddress.toLowerCase()),
  ],

  statusUpdateSchema: [
    check('status')
      .notEmpty()
      .withMessage('status field is required')
      .trim()
      .isIn(['cancelled', 'in-progress', 'ready', 'delivered'])
      .withMessage('invalid value entered')
      .customSanitizer((status) => status.toLowerCase()),
  ],
};
