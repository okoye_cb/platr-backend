import { check } from 'express-validator';

export default {
  customerUpdateSchema: [
    check('firstName')
      .optional()
      .trim()
      .isLength({ min: 2, max: 15 })
      .withMessage('Firstname should be between 2 to 15 characters')
      .isAlpha()
      .withMessage('Firstname should only contain alphabets')
      .customSanitizer((firstname) => firstname.toLowerCase()),

    check('lastName')
      .optional()
      .trim()
      .isLength({ min: 2, max: 15 })
      .withMessage('Lastname should be between 2 to 15 characters')
      .isAlpha()
      .withMessage('Lastname should only contain alphabets')
      .customSanitizer((lastname) => lastname.toLowerCase()),

    check('email')
      .optional()
      .isEmail()
      .withMessage('Enter a valid email address')
      .normalizeEmail(),

    check('phone')
      .optional()
      .trim()
      .not()
      .isEmpty({ ignore_whitespace: true })
      .withMessage('Phonenumber field cannot be blank'),

    check('avatar')
      .optional()
      .not()
      .isString()
      .not()
      .isNumeric()
      .not()
      .isBoolean()
      .not()
      .isArray(),
  ],
};
