import { check } from 'express-validator';

export default {
  categorySchema: [
    check('name')
      .not()
      .isEmpty()
      .withMessage('name is required')
      .trim()
      .isLength({ min: 2, max: 30 })
      .withMessage('name should be between 2 to 30 characters')
      .customSanitizer((name) => name.toLowerCase()),
  ],
};
