import {
  check, oneOf, param, query,
} from 'express-validator';

export default {
  idSchema: [
    oneOf([
      param('customerId').isUUID(),
      param('restaurantId').isUUID(),
    ], 'ID is invalid'),
  ],

  statusSchema: [
    query('status')
      .optional()
      .isIn(['pending', 'rejected', 'active', 'inactive', 'suspended'])
      .withMessage("accepted values: 'pending', 'rejected', 'active', 'inactive', 'suspended'"),
  ],

  regionSchema: [
    query('region')
      .optional()
      .isIn(['Kaduna South', 'Kaduna North'])
      .withMessage('accepted values: Kaduna North, Kaduna South'),
  ],

  statusUpdateSchema: [
    param('restaurantId')
      .not()
      .isEmpty()
      .withMessage('enter restaurantId to continue')
      .isUUID()
      .withMessage('enter a valid restaurantId'),

    check('status')
      .not()
      .isEmpty()
      .withMessage('enter status of restaurant')
      .isIn(['pending', 'rejected', 'active', 'inactive', 'suspended'])
      .withMessage("accepted values: 'pending', 'rejected', 'active', 'inactive', 'suspended'"),
  ],

  addressSchema: [
    check('state')
      .trim()
      .not()
      .isEmpty()
      .withMessage('state is required to continue')
      .equals('Kaduna')
      .withMessage('accepted values: Kaduna'),

    check('region')
      .trim()
      .not()
      .isEmpty()
      .withMessage('region is required to continue')
      .isIn(['Kaduna North', 'Kaduna South'])
      .withMessage('Accepted Regions are Kaduna North and Kaduna South'),

    check('location')
      .trim()
      .not()
      .isEmpty()
      .withMessage('location is required to continue'),
  ],

  addressUpdateSchema: [
    check('state')
      .optional()
      .trim()
      .equals('Kaduna')
      .withMessage('accepted values: Kaduna'),

    check('region')
      .optional()
      .trim()
      .isIn(['Kaduna North', 'Kaduna South'])
      .withMessage('Accepted Regions are Kaduna North and Kaduna South'),

    check('location')
      .optional()
      .trim(),
  ],

  restaurantUpdateSchema: [
    check('businessName')
      .optional(),

    check('email')
      .optional()
      .isEmail()
      .withMessage('Enter a valid email address')
      .normalizeEmail(),

    check('phone')
      .optional(),

    check('address.region')
      .optional()
      .trim()
      .isIn(['Kaduna North', 'Kaduna South'])
      .withMessage('Accepted Regions are Kaduna North and Kaduna South'),

    check('address.state')
      .optional()
      .trim()
      .equals('Kaduna')
      .withMessage('accepted values: Kaduna'),

    check('address.location')
      .optional()
      .trim(),

    check('avatar')
      .optional()
      .not()
      .isString()
      .not()
      .isNumeric()
      .not()
      .isBoolean()
      .not()
      .isArray(),
  ],
};
