import { check } from 'express-validator';

export default {
  emailSchema: [
    check('email')
      .not()
      .isEmpty()
      .withMessage('Email address is required')
      .isEmail()
      .withMessage('Enter a valid email address')
      .normalizeEmail(),
  ],

  passwordSchema: [
    check('password')
      .trim()
      .not()
      .isEmpty({ ignore_whitespace: true })
      .withMessage('Password cannot be blank')
      .isLength({ min: 8 })
      .withMessage('Password should not be less than 8 characters'),
  ],

  restaurantRegistrationSchema: [
    check('businessName')
      .not()
      .isEmpty()
      .withMessage('Provide the business name for your restaurant'),

    check('email')
      .not()
      .isEmpty()
      .withMessage('Email address is required')
      .isEmail()
      .withMessage('Enter a valid email address')
      .normalizeEmail(),

    check('phone')
      .not()
      .isEmpty({ ignore_whitespace: true })
      .withMessage('Phonenumber field cannot be blank'),

    check('password')
      .trim()
      .not()
      .isEmpty({ ignore_whitespace: true })
      .withMessage('Password cannot be blank')
      .isLength({ min: 8 })
      .withMessage('Password should not be less than 8 characters'),
  ],

  restaurantSigninSchema: [
    check('email')
      .not()
      .isEmpty()
      .withMessage('Email address is required')
      .isEmail()
      .withMessage('Enter a valid email address')
      .normalizeEmail(),

    check('password')
      .trim()
      .not()
      .isEmpty({ ignore_whitespace: true })
      .withMessage('Password cannot be blank')
      .isLength({ min: 8 })
      .withMessage('Password should not be less than 8 characters'),
    /* .isAlphanumeric()
      .withMessage('Password must be alphanumeric') */
  ],

  socialLoginSchema: [
    check('provider')
      .trim()
      .exists()
      .withMessage('Provider parameter is required')
      .isIn(['facebook', 'google'])
      .withMessage('Enter a valid provider name'),

    check('accessToken')
      .trim()
      .exists()
      .withMessage('Access token is required'),
  ],

  customerRegistrationSchema: [
    check('firstName')
      .not()
      .isEmpty()
      .withMessage('Firstname is required')
      .trim()
      .isLength({ min: 2, max: 15 })
      .withMessage('Firstname should be between 2 to 15 characters')
      .isAlpha()
      .withMessage('Firstname should only contain alphabets')
      .customSanitizer((firstname) => firstname.toLowerCase()),

    check('lastName')
      .not()
      .isEmpty()
      .withMessage('Lastname is required')
      .trim()
      .isLength({ min: 2, max: 15 })
      .withMessage('Lastname should be between 2 to 15 characters')
      .isAlpha()
      .withMessage('Lastname should only contain alphabets')
      .customSanitizer((lastname) => lastname.toLowerCase()),

    check('email')
      .not()
      .isEmpty()
      .withMessage('Email address is required')
      .isEmail()
      .withMessage('Enter a valid email address')
      .normalizeEmail(),

    check('password')
      .trim()
      .not()
      .isEmpty({ ignore_whitespace: true })
      .withMessage('Password cannot be blank')
      .isLength({ min: 8 })
      .withMessage('Password should be between 8 to 15 characters'),

    check('phone')
      .optional()
      .trim()
      .not()
      .isEmpty({ ignore_whitespace: true })
      .withMessage('Phonenumber field cannot be blank'),
  ],

  customerSigninSchema: [
    check('email')
      .not()
      .isEmpty()
      .withMessage('Email address is required')
      .isEmail()
      .withMessage('Enter a valid email address')
      .normalizeEmail(),

    check('password')
      .trim()
      .not()
      .isEmpty({ ignore_whitespace: true })
      .withMessage('Password cannot be blank')
      .isLength({ min: 8 })
      .withMessage('Password should be between 8 to 15 characters'),
  ],

  changePasswordSchema: [
    check('oldPassword')
      .trim()
      .not()
      .isEmpty({ ignore_whitespace: true })
      .withMessage('Enter your old password')
      .isLength({ min: 8 })
      .withMessage('old password should not be less than 8 characters'),

    check('newPassword')
      .trim()
      .not()
      .isEmpty({ ignore_whitespace: true })
      .withMessage('Enter your new password')
      .isLength({ min: 8 })
      .withMessage('new password should not be less than 8 characters'),
  ],
};
