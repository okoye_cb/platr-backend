import { config } from 'dotenv';
import debug from 'debug';
import paystack from 'paystack-api';

config();

const DEBUG = debug('dev');
const paystackService = paystack(process.env.PAYSTACK_SECRET_KEY);

export default {
  initializeTxn: async (email, amount, otherOptions) => {
    try {
      const { data } = await paystackService.transaction.initialize({
        email,
        amount,
        metadata: {
          custom_fields: [otherOptions],
        },
      });
      return data;
    } catch (error) {
      // TODO: add logger to this part
      DEBUG('paystack error:', error);
      return error;
    }
  },

  chargeTxn: async (email, amount, authorization_code, reference = null) => {
    try {
      const { data } = await paystackService.transaction.chargeAuth({
        email,
        amount,
        authorization_code,
        reference,
      });
      return data;
    } catch (error) {
      // TODO: add logger to this part
      DEBUG('paystack error:', error);
      return error;
    }
  },

  verifyTxn: async (reference) => {
    try {
      const response = await paystackService.transaction.verify({ reference });
      return response;
    } catch (error) {
      // TODO: add logger to this part
      DEBUG('paystack error:', error);
      return error;
    }
  },
};
