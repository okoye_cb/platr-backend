import EventEmitter from 'events';
import emailService from './mailer';

/**
 * @class Notification
 * @description handles all notification related activities. supported activity type
 * are:
 * - accountActivation
 * - passwordRecovery
 * - passwordUpdateSuccessful
 *
 * @extends EventEmitter
 * @exports Notification
 */
class Notification extends EventEmitter {
  /**
   * @description create a notification. initialize the event
   */
  constructor() {
    super();
    this.on('notification', this.handleNotification);
  }

  /**
   * @method handleNotification
   *
   * @param {Object} eventPayload - contains notification type and payload
   * @param {String} eventPayload.type - the type of event
   * @param {Object} eventPayload.payload - the event payload
   *
   * @returns {void}
   */
  handleNotification({ type, payload }) {
    if (type in this) return this[type](payload);
  }

  /**
   * @method addNotification
   *
   * @param {String} type - the type of notification
   * @param {Function} notificationHandler - handles defined notification
   *
   * @returns {Notification} instance of notification
   */
  addNotification(type, notificationHandler) {
    if (!(type in this)) this[type] = notificationHandler.bind(this);
  }

  /**
   * @method sendMail
   * @description handles account notification
   *
   * @param {String} type - type of mail being sent
   * @param {Array} payload - message data
   * @param {String} template - email template to use
   */
  async sendMail(type, payload, template) {
    await emailService({ type, payload, template });
    this.emit('notificationSent', { type, payload });

    return { status: 'sent' };
  }

  /**
   * @method accountRequest
   * @description handles notification when a user creates a vendor account
   *
   * @param {Object} payload - contains necessary info to send the notification
   */
  accountCreated(payload) {
    return this.sendMail('accountCreated', payload);
  }

  /**
   * @method accountActivation
   * @description handles notification for account activation
   *
   * @param {Object} payload - contains necessary info to send the notification
   */
  accountActivation(payload) {
    return this.sendMail('accountActivation', payload);
  }

  /**
   * @method passwordRecovery
   * @description handles notification for password recovery link
   *
   * @param {Array} payload - contains info to send the password reset link
   *
   * @returns {Object} status of event executed
   */
  async passwordRecovery(payload) {
    return this.sendMail('customerPasswordRecovery', payload);
  }

  /**
   * @method passwordResetRestaurant
   * @description handles notification for password recovery link
   *
   * @param {Array} payload - contains info to send the password reset link
   *
   * @returns {Object} status of event executed
   */
  async passwordResetRestaurant(payload) {
    return this.sendMail('vendorPasswordReset', payload);
  }

  /**
   * @method passwordUpdateSuccessful
   * @description handles notification for customers password update successful
   *
   * @param {Array} payload - object that contains the user email
   *
   * @returns {Object} status of event executed
   */
  async passwordUpdateSuccessful(payload) {
    return this.sendMail('customerPasswordUpdated', payload);
  }
}

export default new Notification();
