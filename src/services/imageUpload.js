import cloudinary from 'cloudinary';
import cloudinaryStorage from 'multer-storage-cloudinary';
import dotenv from 'dotenv';
import multer from 'multer';

dotenv.config();

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

const storage = cloudinaryStorage({
  cloudinary,
  folder: 'plater-dev',
  allowedFormats: ['jpg', 'png', 'jpeg'],
  transformation: [{ width: 400, height: 400, crop: 'limit' }],
});
const uploads = multer({ storage });

export default uploads;
