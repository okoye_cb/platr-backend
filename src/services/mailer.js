import { config } from 'dotenv';
import debug from 'debug';
import sgMail from '@sendgrid/mail';

config();

const DEBUG = debug('dev');

const {
  SENDGRID_API_KEY,
  SENDGRID_EMAIL,
  SENDGRID_RESTAURANT_SIGNUP_TEMPLATE,
  SENDGRID_CUSTOMER_PASSWORD_RECOVERY_TEMPLATE,
  SENDGRID_CUSTOMER_PASSWORD_UPDATE_TEMPLATE,
  SENDGRID_VENDOR_PASSWORD_RESET_TEMPLATE,
} = process.env;

const templates = {
  accountCreated: SENDGRID_RESTAURANT_SIGNUP_TEMPLATE,
  customerPasswordRecovery: SENDGRID_CUSTOMER_PASSWORD_RECOVERY_TEMPLATE,
  customerPasswordUpdated: SENDGRID_CUSTOMER_PASSWORD_UPDATE_TEMPLATE,
  vendorPaswordReset: SENDGRID_VENDOR_PASSWORD_RESET_TEMPLATE,
};

/**
 * @function
 * @description
 *
 * @param {Array} payload - can be a list. must contain email parameter
 */
export default async ({ type, payload, template }) => {
  try {
    sgMail.setApiKey(SENDGRID_API_KEY);

    template = template || templates[type];

    if (!template) throw new Error('email template not available');

    if (!Array.isArray(payload)) throw new Error('mail data should be an array');

    const messagePayload = payload.map((singleEmailData) => {
      const { email } = singleEmailData;

      return {
        to: email,
        from: SENDGRID_EMAIL,
        templateId: template,
        dynamic_template_data: { ...singleEmailData },
      };
    });

    await sgMail.send(messagePayload);
  } catch (error) {
    DEBUG(`mailer: ${error}`);
    return error;
  }

  return { message: 'Email sent' };
};
