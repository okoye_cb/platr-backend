import { config } from 'dotenv';
import crypto from 'crypto';
import { ApplicationError, NotFoundError } from '../helpers/errors';
import { createOrFindUser, facebookAuth, googleAuth } from '../services/auth';
import { generateAuthToken } from '../helpers/auth';
import Models from '../models';
import notification from '../services/notification';

config();

const { Customer, Restaurant } = Models;

export default {
  /**
   * @function registerUser
   * @description handles user signup
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} response object
   */
  registerUser: async (request, response) => {
    const existingUser = await Customer.getExistingUser(request.body.email);
    const existingRestaurant = await Restaurant.getExistingRestaurant(request.body.email);
    if (existingUser || existingRestaurant) throw new ApplicationError(409, 'Email already Registered');

    const customer = await Customer.create(request.body);
    const newCustomer = customer.getSafeDataValues();
    const token = generateAuthToken(newCustomer);

    return response.status(201).json({
      status: 'success',
      data: { user: newCustomer, token },
    });
  },

  /**
   * @function signinUser
   * @description handles user login functionality
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} response object
   */
  signinUser: async (request, response) => {
    const { email, password } = request.body;

    const userEmail = await Customer.getExistingUser(email);
    if (!userEmail) {
      throw new ApplicationError(401, 'email or password is incorrect');
    }

    const isPassword = userEmail.validatePassword(password);
    if (!isPassword) {
      throw new ApplicationError(401, 'email or password is incorrect');
    }

    const user = userEmail.getSafeDataValues();
    const token = generateAuthToken(user);

    return response.status(200).json({
      status: 'success',
      data: { user, token },
    });
  },

  /**
   * @function registerRestaurant
   * @description handles restaurant signup request
   *
   * @param {Object} request - the request object
   * @param {Object} response - express response object
   *
   * @returns {Object} response object
   */
  registerRestaurant: async (request, response) => {
    const existingUser = await Customer.getExistingUser(request.body.email);
    const existingRestaurant = await Restaurant.getExistingRestaurant(request.body.email);

    if (existingUser || existingRestaurant) throw new ApplicationError(409, 'Email already Registered');

    const existingBusinessName = await Restaurant.getExistingRestaurant(
      request.body.businessName,
      'businessName',
    );

    if (existingBusinessName) {
      throw new ApplicationError(409, 'business name already taken');
    }

    const restaurant = await Restaurant.create(request.body);

    const newRestaurant = restaurant.getSafeDataValues();
    const { email, businessName } = newRestaurant;
    const token = generateAuthToken(newRestaurant);

    notification.emit('notification', {
      type: 'accountCreated',
      payload: [{
        email,
        businessName,
        verificationLink: `${process.env.FRONT_END_APP_URL}/rauth`,
      }],
    });

    return response.status(201).json({
      status: 'success',
      data: { restaurant: newRestaurant, token },
    });
  },

  /**
   * @function signinToRestaurant
   * @description handles restaurant's owner signin
   *
   * @param {Object} request - the request object
   * @param {Object} response - express response object
   *
   * @returns {Object} response object
   */
  signinToRestaurant: async (request, response) => {
    const { email, password } = request.body;

    const owner = await Restaurant.getExistingRestaurant(email);
    if (!owner) {
      throw new ApplicationError(401, 'email or password is incorrect');
    }
    const isCorrectPassword = owner.validatePassword(password);
    if (!isCorrectPassword) {
      throw new ApplicationError(401, 'email or password is incorrect');
    }

    const token = generateAuthToken(owner);
    const restaurant = owner.getSafeDataValues();

    return response.status(200).json({
      status: 'success',
      data: { restaurant, token },
    });
  },

  /**
   * @deprecated
   * @function socialLogin
   * @description handles social authentication login
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} response object
   */
  socialLogin: async (request, response) => {
    const { provider } = request.params;
    const providerHandler = { facebook: facebookAuth, google: googleAuth };

    const userDetails = await providerHandler[provider](request.body);
    const { status, data } = await createOrFindUser(userDetails);

    return response.status(status).json({
      status: 'success',
      data,
    });
  },

  /**
   * @function changePassword
   * @description allows 'customer' and 'restaurant' user type to change
   * their password
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} response object
   */
  changePassword: async (request, response) => {
    const { id } = request.user;
    const { oldPassword, newPassword } = request.body;
    let user;

    if (request.url.includes('customers')) {
      user = await Customer.findByPk(id);
    } else if (request.url.includes('vendors')) {
      user = await Restaurant.findByPk(id);
    }
    if (!user) throw new NotFoundError();

    const checkOldPassword = user.validatePassword(oldPassword);
    if (!checkOldPassword) {
      throw new ApplicationError(401, 'the old password you entered is wrong');
    }

    const checkNewPassword = user.validatePassword(newPassword);
    if (checkNewPassword) {
      throw new ApplicationError(400, "you can't use the old password again");
    }

    await user.update({ password: newPassword });

    return response.status(200).json({
      status: 'success',
      message: 'password update successful',
    });
  },

  /**
   * @function resetPassword
   * @description handles reset password request for customers and vendors
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} response object
   */
  resetPassword: async (request, response) => {
    const { email } = request.body;

    const restaurant = await Restaurant.getExistingRestaurant(email);
    const customer = await Customer.getExistingUser(email);

    if (!customer && !restaurant) throw new NotFoundError('Incorrect email provided');

    if (customer) {
      const token = await customer.generateVerificationToken();

      const resetLink = `${process.env.FRONT_END_APP_URL}/forgotpassword/${customer.id}?token=${token}`;

      notification.emit('notification', {
        type: 'passwordRecovery',
        payload: [{ email, resetLink }],
      });

      return response.status(200).json({
        status: 'success',
        message: 'email sent successfully',
        data: { resetLink },
      });
    }

    if (restaurant) {
      const password = crypto.randomBytes(10).toString('hex');

      await restaurant.update({ password });

      notification.emit('notification', {
        type: 'passwordResetRestaurant',
        payload: [{ email, password }],
      });

      return response.status(200).json({
        status: 'success',
        message: 'Password reset email sent',
      });
    }
  },

  /**
   * @function updatePassword
   * @description handles update password request for customer
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {void} - returns no data
   */
  updatePassword: async (request, response) => {
    const { id, token } = request.params;
    const { password } = request.body;

    const customer = await Customer.findByPk(id);
    if (!customer) throw new NotFoundError();

    await customer.decodeVerificationToken(token);

    await customer.update({ password });

    notification.emit('notification', {
      type: 'passwordUpdateSuccessful',
      payload: [{
        email: customer.email,
      }],
    });

    return response.status(200).json({
      status: 'success',
      message: 'password updated successfully',
    });
  },
};
