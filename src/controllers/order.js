import { ApplicationError, NotFoundError } from '../helpers/errors';
import asyncForEach from '../helpers/asyncForEach';
import Models from '../models';
import paystackService from '../services/paystack';

const {
  Order,
  Product,
  Customer,
  Restaurant,
} = Models;

const productExist = async (productID) => {
  const existingProduct = await Product.findByPk(productID);
  if (existingProduct) return existingProduct;
  throw new NotFoundError(
    `Product with the given id ${productID} does not exists`,
  );
};

export default {
  /**
   * @function addOrder
   * @description handles creating a new order
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} response object
   */
  addOrder: async (request, response) => {
    const orderRequest = request.body;
    const { products } = request.body;
    const { user } = request;

    if (!(user instanceof Customer)) {
      throw new ApplicationError(
        401,
        'Please sign in as a customer or use the anonymous route',
      );
    }

    // check for the existence of all products on order request and retrieve same
    const arrayOfProductObject = await Promise.all(
      products.map((product) => productExist(product.id)),
    );

    const restaurant = await Restaurant.findByPk(
      arrayOfProductObject[0].restaurantId,
    );

    // create order for current user
    const order = await user.createOrder({
      deliveryFee: orderRequest.deliveryFee,
      restaurantId: arrayOfProductObject[0].restaurantId,
      customerName: `${user.firstName} ${user.lastName}`,
      customerEmail: user.email,
      customerPhone: user.phone,
      deliveryAddress: orderRequest.deliveryAddress,
    });

    // update the order by adding product(s)
    await asyncForEach(products, async (product, index) => {
      await order.addProduct(arrayOfProductObject[index], {
        through: {
          quantity: product.quantity,
          subtotal: arrayOfProductObject[index].minOrderPrice * product.quantity,
        },
      });
    });

    // calculate and update totalPrice for this order
    const allProductOnOrder = await order.getProducts();
    order.totalPrice = allProductOnOrder.reduce(
      (total, product) => total + product.OrderProduct.subtotal, 0,
    );
    order.totalPrice += Number(order.deliveryFee);
    // calculate and update totalQuantity for this order
    order.totalQuantity = allProductOnOrder.reduce(
      (total, product) => total + product.OrderProduct.quantity, 0,
    );

    // paystack from here
    const { authorization_url, reference } = await paystackService.initializeTxn(
      order.customerEmail,
      order.totalPrice * 100,
      {
        display_name: 'Restaurant',
        variable_name: 'restaurant',
        value: restaurant.businessName,
      },
    );

    order.txnReference = reference;
    await order.save();

    return response.status(200).json({
      status: 'success',
      message: 'Authorization URL generated',
      data: { url: authorization_url },
    });
  },

  /**
   * @function addAnonymousOrder
   * @description handles creating a new order
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} response object
   */
  addAnonymousOrder: async (request, response) => {
    const orderRequest = request.body;
    const { products } = request.body;

    // check for the existence of all products on order request and retrieve same
    const arrayOfProductObject = await Promise.all(
      products.map((product) => productExist(product.id)),
    );

    // get the restaurant to order from
    const restaurant = await Restaurant.findByPk(
      arrayOfProductObject[0].restaurantId,
    );

    // create order for current user
    const order = await restaurant.createOrder({
      customerName: orderRequest.customerName,
      customerEmail: orderRequest.customerEmail,
      customerPhone: orderRequest.customerPhone,
      deliveryAddress: orderRequest.deliveryAddress,
      deliveryFee: orderRequest.deliveryFee,
      restaurantId: arrayOfProductObject[0].restaurantId,
    });

    // update the order by adding product(s)
    await asyncForEach(products, async (product, index) => {
      await order.addProduct(arrayOfProductObject[index], {
        through: {
          quantity: product.quantity,
          subtotal: arrayOfProductObject[index].minOrderPrice * product.quantity,
        },
      });
    });

    // calculate and update totalPrice for this order
    const allProductOnOrder = await order.getProducts();
    order.totalPrice = allProductOnOrder.reduce(
      (total, product) => total + product.OrderProduct.subtotal,
      0,
    );
    order.totalPrice += Number(orderRequest.deliveryFee);
    await order.save();

    // calculate and update totalQuantity for this order
    order.totalQuantity = allProductOnOrder.reduce(
      (total, product) => total + product.OrderProduct.quantity, 0,
    );

    // paystack from here
    const { authorization_url, reference } = await paystackService.initializeTxn(
      order.customerEmail,
      order.totalPrice * 100,
      {
        display_name: 'Restaurant',
        variable_name: 'restaurant',
        value: restaurant.businessName,
      },
    );

    order.txnReference = reference;
    await order.save();

    return response.status(200).json({
      status: 'success',
      message: 'Authorization URL generated',
      data: { url: authorization_url },
    });
  },

  /**
   * @function verifyOrderPayment
   * @description
   *
   * @param {Object} request
   * @param {Object} response
   *
   * @returns {Object} response object
   */
  verifyOrderPayment: async (request, response) => {
    const { reference } = request.query;

    const order = await Order.findOne({ where: { txnReference: reference } });
    if (!order) {
      return response.status(400).json({
        status: 'error',
        message: 'Invalid transaction reference.',
      });
    }

    const result = await paystackService.verifyTxn(reference);
    if (result.data.status === 'failed' || !result.status) {
      order.paymentStatus = 'failed';
      order.status = 'cancelled';

      let errorMsg;
      if (result.data) {
        errorMsg = result.data.gateway_response;
      } else {
        errorMsg = result.message;
      }

      return response.status(400).json({
        status: 'error',
        message: errorMsg,
      });
    }
    order.paymentStatus = 'successful';
    order.status = 'in-progress';
    await order.save();

    return response.redirect('https://platr.ng');
  },

  /**
   * @function updateOrderStatus
   * @description handles creating a new order
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} response object
   */
  updateOrderStatus: async (request, response) => {
    const { id } = request.params;
    const { status } = request.body;
    const { user } = request;

    if (!(user instanceof Restaurant)) {
      throw new ApplicationError(401, 'Please sign in as a restaurant');
    }

    // get the order to update
    let order = await Order.findByPk(id);
    if (!order) throw new NotFoundError(`No such order with the given id: ${id}`);

    // update the order status
    order.status = status;
    await order.save();

    // query for the order to include list of products on it
    order = await Order.findByPk(order.id, {
      include: {
        model: Product,
        through: { attributes: ['quantity', 'subtotal'] },
      },
    });

    // return the updated order
    return response.status(200).json({
      status: 'success',
      data: { order },
    });
  },

  /**
   * @function getOrders All Order
   * @description handles getting all order for a particular
   * Restaurant/Customer account
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} response object
   */
  getOrders: async (request, response) => {
    const { user } = request;

    const orders = await user.getOrders({
      include: {
        model: Product,
        through: { attributes: ['quantity', 'subtotal'] },
      },
    });

    return response.status(200).json({
      status: 'success',
      data: { orders },
    });
  },

  /**
   * @function getOrder
   * @description
   *
   * @param {Object} request
   * @param {Object} response
   *
   * @returns
   */
  getOrder: async (request, response) => {
    const { user, params: { id } } = request;

    const order = await user.getOrders({
      where: { id },
      include: {
        model: Product,
        through: { attributes: ['quantity', 'subtotal'] },
      },
    });
    if (!order.length) {
      throw new NotFoundError(`No such order with the given id: ${id}`);
    }

    return response.status(200).json({
      status: 'success',
      data: { order: order[0] },
    });
  },

  /**
   * @function getOrderStatus
   * @description
   *
   * @param {Object} request
   * @param {Object} response
   *
   * @returns
   */
  getOrderStatus: async (request, response) => {
    const { id: txnReference } = request.params;

    const order = await Order.findOne({
      where: { txnReference },
      attributes: ['status'],
    });
    if (!order) {
      throw new NotFoundError(`No such order with the given id: ${txnReference}`);
    }

    return response.status(200).json({
      status: 'success',
      data: { order },
    });
  },
};
