import Models from '../models';
import paginator from '../helpers/paginator';

const { Order, Restaurant } = Models;

export default {
  getOrders: async (request, response) => {
    const { page = 1, limit = 10 } = request.query;

    const { data, count } = await paginator(Order, {
      where: { status: 'ready' },
      attributes: ['customerName', 'deliveryAddress', 'status', 'createdAt'],
      include: [{
        model: Restaurant,
        as: 'restaurant',
        attributes: ['businessName'],
      }],
      page,
      limit,
    });

    return response.status(200).json({
      status: 'success',
      message: 'Orders fetched successfully',
      data,
      count,
      page: +page,
      limit: +limit,
    });
  },
};
