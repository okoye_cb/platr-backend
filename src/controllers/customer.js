import { ApplicationError, NotFoundError } from '../helpers/errors';
import Models from '../models';
import paginator from '../helpers/paginator';

const { Customer } = Models;

export default {
  /**
   * @function getAllUsers
   * @description retrieves all users from the database
   *
   * @param {Object} request
   * @param {Object} response
   *
   * @returns {Object}
   */
  getAllCustomers: async (request, response) => {
    const { page = 1, limit = 10 } = request.query;

    const { data, count } = await paginator(Customer, { page, limit });

    return response.status(200).json({
      status: 'success',
      data: JSON.parse(JSON.stringify(data)).map((item) => {
        const { password, ...dataValues } = item;
        return dataValues;
      }),
      count,
      page: +page,
      limit: +limit,
    });
  },

  /**
   * @function getOneCustomer
   * @description retrieve a single customer
   *
   * @param {Object} request
   * @param {Object} response
   *
   * @returns {Object}
   */
  getOneCustomer: async (request, response) => {
    const { customerId } = request.params;

    const customer = await Customer.findByPk(customerId, {
      attributes: { exclude: ['password'] },
    });

    if (!customer) throw new NotFoundError('customer not found');

    return response.status(200).json({
      status: 'success',
      data: customer,
    });
  },

  /**
   * @function getCustomerProfile
   * @description retrieve profile by a "customer" user type
   *
   * @param {Object} request
   * @param {Object} response
   *
   * @returns {Object}
   */
  getCustomerProfile: async (request, response) => {
    const { id } = request.user;

    const profile = await Customer.findByPk(id);

    return response.status(200).json({
      status: 'success',
      data: profile,
    });
  },

  /**
   * @function updateCustomer
   * @description handles edit customer details
   *
   * @param {Object} request - the request object
   * @param {Object} response - express response object
   *
   * @returns {Object} response object
   */
  updateCustomer: async (request, response) => {
    const { user } = request;
    const { id } = user.dataValues;

    const customer = await Customer.findByPk(id);
    if (!customer) throw new NotFoundError('User not found');

    const { password, status } = request.body;
    if (password) throw new ApplicationError(403, 'You cannot update your password through this route');

    if (request.file) {
      const { secure_url } = request.file;
      request.body.avatar = secure_url;
    }

    const updatedCustomer = await customer.update(request.body, {
      fields: Object.keys(request.body),
    });

    response.status(200).json({
      status: 'success',
      data: { customer: updatedCustomer },
    });
  },
};
