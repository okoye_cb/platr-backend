import { Op } from 'sequelize';
import { ApplicationError, NotFoundError } from '../helpers/errors';
import Models from '../models';

const { Category } = Models;

export default {
  /**
   * @function addCategory
   * @description handles creating a new category
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} response object
   */
  addCategory: async (request, response) => {
    const { user, role } = request;
    const { name } = request.body;
    const restaurantId = user.id;

    if (role !== 'vendor') {
      throw new ApplicationError(403, 'You are not permitted');
    }
    const nameExist = await Category.findOne({ where: { name } });
    if (nameExist) {
      throw new ApplicationError(409, 'Category already exists');
    }
    const savedCategory = await user.createCategory({ ...request.body });

    return response.status(201).json({
      status: 'success',
      data: { category: savedCategory },
    });
  },

  /**
   * @function getCategoryDishes
   * @description handles getting dishes in a category
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} response object
   */
  getCategoryDishes: async (request, response) => {
    const categoryId = request.params.id;

    const category = await Category.findByPk(categoryId);
    if (!category) {
      throw new NotFoundError('This category does not exist');
    }

    const products = await category.getProducts();

    return response.status(200).json({
      status: 'success',
      data: { products },
    });
  },

  /**
   * @function getRestaurantCategoryDishes
   * @description handles getting dishes from a restaurant in a category
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} response object
   */
  getRestaurantCategoryDishes: async (request, response) => {
    const { name, restaurantId } = request.params;

    const categories = await Category.findAll({
      where: {
        restaurantId,
        name: { [Op.like]: `%${name}%` },
      },
    });

    if (categories.length) {
      Promise.all(
        categories.map(async (category) => {
          const restaurantProducts = await category.getProducts();
          return restaurantProducts;
        }),
      ).then((product) => {
        response.status(200).json({
          status: 'success',
          data: { product },
        });
      });
    } else {
      throw new NotFoundError('There are no products in this category');
    }
  },

  /**
   * @function get All Categories
   * @description handles getting all categories from db
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} response object
   */
  getAllCategory: async (request, response) => {
    const allCategory = await Category.findAll({});

    return response.status(200).json({
      status: 'success',
      data: { category: allCategory },
    });
  },
};
