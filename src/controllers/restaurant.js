import {
  where, fn, col,
} from 'sequelize';
import { ApplicationError, NotFoundError } from '../helpers/errors';
import Models from '../models';
import paginator from '../helpers/paginator';

const { Restaurant, Address } = Models;

export default {
  /**
   * @function getRestaurants
   * @description gets restaurants from db
   *
   * @param {Object} request - the request object
   * @param {Object} response - express response object
   *
   * @returns {Object} response object
   */
  getRestaurants: async (request, response) => {
    const {
      page = 1, limit = 10, status, region,
    } = request.query;

    const query = { where: {} };

    const address = {
      include: [{
        model: Address,
        attributes: ['region', 'location', 'state'],
      }],
    };

    if (status) query.where.status = status;
    if (region) {
      const lookupValue = region.toLowerCase();
      query.where['$Address.region$'] = where(fn('LOWER', col('region')), 'LIKE', `%${lookupValue}%`);
    }

    const { data, count } = await paginator(Restaurant, {
      page, limit, ...query, ...address,
    });

    return response.status(200).json({
      status: 'success',
      data: {
        restaurants: JSON.parse(JSON.stringify(data)).map((item) => {
          const { password, ...dataValues } = item;
          return dataValues;
        }),
      },
      count,
      page: +page,
      limit: +limit,
    });
  },

  /**
   * @function getOneRestaurant
   * @description gets a single restaurant
   *
   * @param {Object} request - the request object
   * @param {Object} response - express response object
   *
   * @returns {Object} response object
   */
  getOneRestaurant: async (request, response) => {
    const restaurant = await Restaurant.findByPk(
      request.params.restaurantId,
      {
        include: [Address],
        attributes: { exclude: ['password'] },
      },
    );

    if (!restaurant) {
      throw new NotFoundError('Restaurant not found');
    }

    return response.status(200).json({
      status: 'success',
      data: { restaurant },
    });
  },

  /**
   * @function addAddress
   * @description handles restaurant address creation by owner
   *
   * @param {Object} request - the request object
   * @param {Object} response - express response object
   *
   * @returns {Object} response object
   */
  addAddress: async (request, response) => {
    const { user } = request;
    const { id } = user.dataValues;
    let hasAddress;

    if (request.role === 'vendor') {
      hasAddress = await Address.findOne({
        where: { restaurantId: id },
      });
      request.body.restaurantId = id;
    } else if (request.role === 'customer') {
      hasAddress = await Address.findOne({
        where: { customerId: id },
      });
      request.body.customerId = id;
    }

    if (hasAddress) {
      throw new ApplicationError(
        409,
        'Address already exists for this resource, Please update instead.',
      );
    }

    const address = await Address.create(request.body);
    await user.setAddress(address);

    return response.status(201).json({
      status: 'success',
      data: { address },
    });
  },

  /**
   * @function addAddress
   * @description handles fetching restaurant address by users
   *
   * @param {Object} request - the request object
   * @param {Object} response - express response object
   *
   * @returns {Object} response object
   */
  getAddress: async (request, response) => {
    const restaurantId = request.params.id;

    const address = await Address.findOne({ where: { restaurantId } });
    if (!address) {
      throw new NotFoundError('Address not found for this restaurant');
    }

    response.status(200).json({
      status: 'success',
      data: { address },
    });
  },

  /**
   * @function getRestaurantProfile
   * @description retrieve restaurant profile information
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} restaurant user record
   */
  getRestaurantProfile: async (request, response) => {
    const { id } = request.user;

    const profile = await Restaurant.findByPk(
      id, { include: [Address], attributes: { exclude: ['password'] } },
    );

    return response.status(200).json({
      status: 'success',
      data: { restaurant: profile },
    });
  },

  /**
   * @deprecated
   * @function updateAddress
   * @description handles restaurant address update by owner
   *
   * @param {Object} request - the request object
   * @param {Object} response - express response object
   *
   * @returns {Object} response object
   */
  updateAddress: async (request, response) => {
    const { user } = request;
    const { id } = user.dataValues;

    let address;
    if (request.role === 'vendor') {
      address = await Address.findOne({
        where: { restaurantId: id },
      });
      request.body.restaurantId = id;
    } else if (request.role === 'customer') {
      address = await Address.findOne({
        where: { customerId: id },
      });
      request.body.customerId = id;
    }

    if (!address) {
      throw new NotFoundError(
        'This restaurant does not have an address, please create one',
      );
    }

    const updatedAddress = await address.update(request.body, {
      fields: Object.keys(request.body),
    });

    response.status(200).json({
      status: 'success',
      data: { address: updatedAddress },
    });
  },

  /**
   * @function updateRestaurant
   * @description handles edit restaurant details
   *
   * @param {Object} request - the request object
   * @param {Object} response - express response object
   *
   * @returns {Object} response object
   */
  updateRestaurant: async (request, response) => {
    const { user } = request;
    const { id } = user.dataValues;

    const restaurant = await Restaurant.findByPk(id);
    if (!restaurant) throw new NotFoundError('Restaurant not found');

    const { password, status } = request.body;
    if (password || status) throw new ApplicationError(403, 'You cannot update your password or status through this route');

    if (request.file) {
      const { secure_url } = request.file;
      request.body.avatar = secure_url;
    }

    if ('address' in request.body) {
      const existingAddress = await restaurant.getAddress();
      if (existingAddress) {
        await existingAddress.update(request.body.address, {
          fields: Object.keys(request.body.address),
        });
      } else {
        await restaurant.createAddress(request.body.address);
      }
      delete request.body.address; // very necessary
    }

    const updatedRestaurant = await restaurant.update(request.body, {
      fields: Object.keys(request.body),
    });

    const address = await updatedRestaurant.getAddress();
    updatedRestaurant.dataValues.Address = address ? address.dataValues : null;

    return response.status(200).json({
      status: 'success',
      data: { restaurant: updatedRestaurant },
    });
  },

  /**
   * @function updateRestaurantStatus
   * @description for admin user only. responsible for vendor's status
   * accepted values: 'pending', 'rejected', 'active', 'inactive', 'suspended'
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} response object
   */
  updateRestaurantStatus: async (request, response) => {
    const { restaurantId } = request.params;
    const { status } = request.body;

    const restaurant = await Restaurant.findOne({ where: { id: restaurantId } });
    if (!restaurant) throw new NotFoundError();

    const restaurantStatus = await restaurant.update(
      { status },
      { fields: ['status'] },
    );

    return response.status(200).json({
      status: 'success',
      data: { status: restaurantStatus },
    });
  },

  /**
   * @function deleteAddress
   * @description handles restaurant address deletion by owner
   *
   * @param {Object} request - the request object
   * @param {Object} response - express response object
   *
   * @returns {Object} response object
   */
  deleteAddress: async (request, response) => {
    const { user } = request;
    const { id } = user.dataValues;

    let address;
    if (request.role === 'vendor') {
      address = await Address.destroy({
        where: { restaurantId: id },
      });
    } else if (request.role === 'customer') {
      address = await Address.destroy({
        where: { customerId: id },
      });
    }

    if (!address) {
      throw new NotFoundError('This restaurant does not have an address');
    }

    return response.status(204).send({
      status: 'success',
      message: 'Address deleted successfully',
    });
  },
};
