import { ApplicationError, NotFoundError } from '../helpers/errors';
import Models from '../models';
import paginator from '../helpers/paginator';

const { Product, Category, Restaurant } = Models;

export default {
  /**
   * @function createDish
   * @description handles creating a new dish
   *
   * @param {Object} request - the request object
   * @param {Object} response - the response object
   *
   * @returns {Object} response object
   */
  createDish: async (request, response) => {
    if (request.files) {
      request.body.imageURL = request.files[0].secure_url;
    }

    const { name, categoryId } = request.body;
    const restaurant = request.user;
    const restaurantId = restaurant.dataValues.id;

    if (request.role !== 'vendor') {
      throw new ApplicationError(403, 'You are not allowed to perform operation');
    }

    const nameExist = await Product.findOne({
      where: { name, restaurantId },
    });
    if (nameExist) {
      throw new ApplicationError(409, 'Dish already exists for this restaurant');
    }

    const category = await Category.findByPk(categoryId);
    if (!category) throw new NotFoundError(`No category with id: ${categoryId}`);

    const savedDish = await restaurant.createProduct({ ...request.body });

    return response.status(201).json({
      status: 'success',
      data: { dish: savedDish },
    });
  },

  /**
   * @function getAllDish
   * @description handles fetching all dishes created by restaurant owners
   *
   * @param {Object} request - the request object
   * @param {Object} response - express response object
   *
   * @returns {Object} response object
   */
  getAllDish: async (request, response) => {
    const { page = 1, limit = 10 } = request.query;

    const { data, count } = await paginator(Product, { page, limit });

    return response.status(200).json({
      status: 'success',
      data,
      count,
      page: +page,
      limit: +limit,
    });
  },

  /**
   * @function getVendorDishes
   * @description handles fetching all dishes owned by a vendor
   *
   * @param {Object} request - the request object
   * @param {Object} response - express response object
   *
   * @returns {Object} response object
   */
  getVendorDishes: async (request, response) => {
    const { restaurantId } = request.params;

    const restaurant = await Restaurant.findByPk(restaurantId);
    if (!restaurant) throw new NotFoundError('Restaurant not found');

    const products = await restaurant.getProducts();
    if (products.length <= 0) {
      throw new NotFoundError('There are no products for this restaurant');
    }

    return response.status(200).json({
      status: 'success',
      data: { products },
    });
  },

  /**
   * @function getOneDish
   * @description handles fetching a specific dish created by restaurant owners
   *
   * @param {Object} request - the request object
   * @param {Object} response - express response object
   *
   * @returns {Object} response object
   */
  getOneDish: async (request, response) => {
    const specificDish = await Product.findByPk(request.params.id);
    if (specificDish) {
      return response.status(200).json({
        status: 'success',
        dish: specificDish,
      });
    }
    throw new NotFoundError('Dish Not Found');
  },

  /**
   * @function updateDish
   * @description handles update dish by restaurant owner
   *
   * @param {Object} request - the request object
   * @param {Object} response - express response object
   *
   * @returns {Object} response object
   */
  updateDish: async (request, response) => {
    const { id } = request.params;
    const restaurantId = request.user.dataValues.id;

    if (request.role !== 'vendor') {
      throw new ApplicationError(403, 'You are not permitted');
    }
    if (request.files.length) {
      request.body.imageURL = request.files[0].secure_url;
    }

    const product = await Product.findOne({
      where: { id, restaurantId },
    });
    const updatedDish = await product.update(request.body, {
      fields: Object.keys(request.body),
    });

    if (updatedDish) {
      return response.status(200).json({
        status: 'success',
        data: { updated: updatedDish },
      });
    }
    throw new NotFoundError('Dish not found');
  },

  /**
   * @function deleteDish
   * @description handles dish deletion by restaurant owner
   *
   * @param {Object} request - the request object
   * @param {Object} response - express response object
   *
   * @returns {Object} response object
   */
  deleteDish: async (request, response) => {
    const { id } = request.params;

    const numberOfDishRemoved = await Product.destroy({ where: { id } });
    if (numberOfDishRemoved === 1) {
      return response.status(200).json({
        status: 'success',
        data: { message: 'Dish deleted successfully' },
      });
    }
    throw new NotFoundError('Dish Not Found');
  },
};
