import { Router } from 'express';
import asyncWrapper from '../middleware/asyncWrapper';
import authentication from '../middleware/authenticate';
import customerController from '../controllers/customer';
import customerSchemas from '../validations/customer';
import restaurantSchema from '../validations/restaurant';
import uploads from '../services/imageUpload';
import validator from '../middleware/validator';

const router = Router();

const {
  getAllCustomers,
  getOneCustomer,
  getCustomerProfile,
  updateCustomer,
} = customerController;
const { customerUpdateSchema } = customerSchemas;
const { idSchema } = restaurantSchema;
const { verifyToken, isAdmin } = authentication;

router.get(
  '/profile',
  verifyToken,
  asyncWrapper(getCustomerProfile),
);

router.get(
  '/',
  verifyToken,
  isAdmin,
  asyncWrapper(getAllCustomers),
);

router.get(
  '/:customerId',
  verifyToken,
  isAdmin,
  validator(idSchema),
  asyncWrapper(getOneCustomer),
);

router.put(
  '/me',
  verifyToken,
  uploads.single('avatar'),
  validator(customerUpdateSchema),
  asyncWrapper(updateCustomer),
);

export default router;
