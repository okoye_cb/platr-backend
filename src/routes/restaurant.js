import { Router } from 'express';
import asyncWrapper from '../middleware/asyncWrapper';
import authenticate from '../middleware/authenticate';
import productController from '../controllers/product';
import restaurantController from '../controllers/restaurant';
import restaurantSchemas from '../validations/restaurant';
import validator from '../middleware/validator';
import uploads from '../services/imageUpload';

const { getVendorDishes } = productController;

const {
  addAddress,
  getAddress,
  getRestaurants,
  getOneRestaurant,
  getRestaurantProfile,
  updateRestaurant,
  updateAddress,
  updateRestaurantStatus,
  deleteAddress,
} = restaurantController;

const {
  addressSchema,
  addressUpdateSchema,
  idSchema,
  statusSchema,
  regionSchema,
  statusUpdateSchema,
  restaurantUpdateSchema,
} = restaurantSchemas;

const {
  permit,
  verifyToken,
} = authenticate;

const router = Router();

router.post(
  '/myAddress',
  verifyToken,
  validator(addressSchema),
  asyncWrapper(addAddress),
);

router.get(
  '/',
  validator(statusSchema),
  validator(regionSchema),
  asyncWrapper(getRestaurants),
);

router.get(
  '/:restaurantId',
  validator(idSchema),
  asyncWrapper(getOneRestaurant),
);

router.get(
  '/profile/me',
  verifyToken,
  asyncWrapper(getRestaurantProfile),
);

router.get(
  '/:restaurantId/products',
  asyncWrapper(getVendorDishes),
);

router.get(
  '/:id/address',
  asyncWrapper(getAddress),
);

router.patch(
  '/me',
  verifyToken,
  uploads.single('avatar'),
  validator(restaurantUpdateSchema),
  asyncWrapper(updateRestaurant),
);

router.patch(
  '/:restaurantId/status',
  verifyToken,
  permit('admin', 'vendor'),
  validator(statusUpdateSchema),
  asyncWrapper(updateRestaurantStatus),
);

router.delete(
  '/myAddress',
  verifyToken,
  asyncWrapper(deleteAddress),
);

export default router;
