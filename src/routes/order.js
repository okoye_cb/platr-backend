import { Router } from 'express';
import asyncWrapper from '../middleware/asyncWrapper';
import authenticate from '../middleware/authenticate';
import orderController from '../controllers/order';
import orderSchema from '../validations/order';
import validator from '../middleware/validator';

const { verifyToken } = authenticate;

const {
  orderCreationSchema,
  statusUpdateSchema,
  anonymousOrderCreationSchema,
} = orderSchema;

const {
  addOrder,
  addAnonymousOrder,
  getOrders,
  getOrder,
  getOrderStatus,
  updateOrderStatus,
  verifyOrderPayment,
} = orderController;

const router = Router();

router.post(
  '/',
  verifyToken,
  validator(orderCreationSchema),
  asyncWrapper(addOrder),
);

router.post(
  '/anonymous',
  validator(anonymousOrderCreationSchema),
  asyncWrapper(addAnonymousOrder),
);

router.get(
  '/paystack/callback',
  verifyOrderPayment,
);

router.get(
  '/',
  verifyToken,
  asyncWrapper(getOrders),
);

router.get(
  '/status/:id',
  asyncWrapper(getOrderStatus),
);

router.get(
  '/:id',
  verifyToken,
  asyncWrapper(getOrder),
);

router.put(
  '/:id',
  verifyToken,
  validator(statusUpdateSchema),
  asyncWrapper(updateOrderStatus),
);

export default router;
