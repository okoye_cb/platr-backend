import { Router } from 'express';
import asyncWrapper from '../middleware/asyncWrapper';
import authenticate from '../middleware/authenticate';
import categoryController from '../controllers/category';
import schemas from '../validations/category';
import validator from '../middleware/validator';

const { verifyToken } = authenticate;

const {
  addCategory,
  getAllCategory,
  getCategoryDishes,
  getRestaurantCategoryDishes,
} = categoryController;
const { categorySchema } = schemas;

const router = Router();

router.post(
  '/', verifyToken,
  validator(categorySchema),
  asyncWrapper(addCategory),
);

router.get(
  '/',
  asyncWrapper(getAllCategory),
);

router.get(
  '/:id',
  asyncWrapper(getCategoryDishes),
);

router.get(
  '/:restaurantId/:name',
  asyncWrapper(getRestaurantCategoryDishes),
);

export default router;
