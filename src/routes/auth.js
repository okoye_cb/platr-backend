import { Router } from 'express';
import asyncWrapper from '../middleware/asyncWrapper';
import authController from '../controllers/auth';
import authentication from '../middleware/authenticate';
import schemas from '../validations/auth';
import validator from '../middleware/validator';

const {
  registerUser,
  signinUser,
  registerRestaurant,
  signinToRestaurant,
  changePassword,
  resetPassword,
  userResetPassword,
  updatePassword,
} = authController;

const { verifyToken } = authentication;

const {
  emailSchema,
  passwordSchema,
  customerRegistrationSchema,
  customerSigninSchema,
  restaurantRegistrationSchema,
  restaurantSigninSchema,
  changePasswordSchema,
} = schemas;

const router = Router();

router.post(
  '/customers/signup',
  validator(customerRegistrationSchema),
  asyncWrapper(registerUser),
);

router.post(
  '/customers/signin',
  validator(customerSigninSchema),
  asyncWrapper(signinUser),
);

router.post(
  '/vendor/reset_password',
  validator(emailSchema),
  asyncWrapper(resetPassword),
);

router.post(
  '/reset_password',
  validator(emailSchema),
  asyncWrapper(userResetPassword),
);

router.post(
  '/vendors/signup',
  validator(restaurantRegistrationSchema),
  asyncWrapper(registerRestaurant),
);

router.post(
  '/vendors/signin',
  validator(restaurantSigninSchema),
  asyncWrapper(signinToRestaurant),
);

router.patch(
  '/vendors/change_password',
  verifyToken,
  validator(changePasswordSchema),
  asyncWrapper(changePassword),
);

router.patch(
  '/customers/change_password',
  verifyToken,
  validator(changePasswordSchema),
  asyncWrapper(changePassword),
);

router.patch(
  '/update_password/:id/:token',
  validator(passwordSchema),
  asyncWrapper(updatePassword),
);

export default router;
