import { Router } from 'express';
import asyncWrapper from '../middleware/asyncWrapper';
import authenticate from '../middleware/authenticate';
import productController from '../controllers/product';
import schemas from '../validations/product';
import Uploads from '../services/imageUpload';
import validator from '../middleware/validator';

const { verifyToken } = authenticate;
const {
  createDish,
  getAllDish,
  getOneDish,
  updateDish,
  deleteDish,
} = productController;
const {
  productSchema,
  productUpdateSchema,
} = schemas;

const router = Router();

router.post(
  '/',
  verifyToken,
  Uploads.any(),
  validator(productSchema),
  asyncWrapper(createDish),
);

router.get(
  '/',
  asyncWrapper(getAllDish),
);

router.get(
  '/:id',
  asyncWrapper(getOneDish),
);

router.patch(
  '/:id',
  verifyToken,
  Uploads.any(),
  validator(productUpdateSchema),
  asyncWrapper(updateDish),
);

router.delete(
  '/:id',
  verifyToken,
  asyncWrapper(deleteDish),
);

export default router;
