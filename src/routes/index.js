import { Router } from 'express';
import authRoutes from './auth';
import categoryRoutes from './category';
import customerRoutes from './customer';
import logisticsRoutes from './logistics';
import orderRoutes from './order';
import productRoutes from './product';
import restaurantRoutes from './restaurant';

const router = Router();

router.use('/auth', authRoutes);
router.use('/vendors', restaurantRoutes);
router.use('/customers', customerRoutes);
router.use('/products', productRoutes);
router.use('/categories', categoryRoutes);
router.use('/orders', orderRoutes);
router.use('/logistics', logisticsRoutes);

export default router;
