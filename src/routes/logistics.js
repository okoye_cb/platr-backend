import { Router } from 'express';
import asyncWrapper from '../middleware/asyncWrapper';
import logisticsController from '../controllers/logistics';

const { getOrders } = logisticsController;

const router = Router();

router.get(
  '/orders',
  asyncWrapper(getOrders),
);

export default router;
