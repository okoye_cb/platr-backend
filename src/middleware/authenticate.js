import { config } from 'dotenv';
import jwt from 'jsonwebtoken';
import { promisify } from 'util';
import { ApplicationError, NotFoundError } from '../helpers/errors';
import asyncWrapper from './asyncWrapper';
import Models from '../models';

config();

const { Customer, Restaurant } = Models;

export default {
  /**
   * @param {Object} request express request object
   * @param {Object} response express response object
   * @param {Function} next callback to call next middleware
   *
   * @returns {Object} response from the server
   */
  verifyToken: asyncWrapper(
    async (request, response, next) => {
      const { JWT_KEY } = process.env;
      const authHeader = request.headers.authorization || request.headers['x-access-token'];

      if (!authHeader) {
        return next(
          new ApplicationError(401, 'No token provided. Please signup or login'),
        );
      }

      let token;

      if (authHeader.startsWith('Bearer ')) {
        [, token] = authHeader.split(' ');
      } else {
        token = authHeader;
      }
      const decoded = await promisify(jwt.verify)(token, JWT_KEY);

      const currentRestaurant = await Restaurant.findByPk(decoded.id);
      const currentCustomer = await Customer.findByPk(decoded.id);

      if (currentRestaurant) {
        request.user = currentRestaurant;
        request.role = 'vendor';
      } else if (currentCustomer) {
        if (currentCustomer.isAdmin) {
          request.role = 'admin';
        } else {
          request.role = 'customer';
        }
        request.user = currentCustomer;
      } else {
        return next(new NotFoundError('Invalid credentials, please log in again'));
      }
      return next();
    },
  ),

  /**
   * @description checks if user is an admin
   *
   * @param {Object} request express request object
   * @param {Object} response express response object
   * @param {Function} callback to next middleware
   *
   * @returns {Object}
   */
  isAdmin: (request, response, next) => {
    const { isAdmin } = request.user;
    if (!isAdmin) {
      throw new ApplicationError(403, 'unauthorized. for admin account only');
    }
    next();
  },

  /**
   * @description
   *
   * @param {String}
   *
   * @returns {}
   */
  permit: (...permitted) => (request, response, next) => {
    if (permitted.indexOf(request.role) !== -1) return next();
    throw new ApplicationError(403, 'access denied');
  },
};
