export default (sequelize, DataTypes) => {
  const address = sequelize.define('Address', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
    },
    state: DataTypes.STRING,
    region: DataTypes.STRING,
    location: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
  }, {});

  address.associate = (models) => {
    address.belongsTo(models.Restaurant, {
      foreignKey: {
        name: 'restaurantId',
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    });
    address.belongsTo(models.Customer, {
      foreignKey: {
        name: 'customerId',
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    });
  };

  return address;
};
