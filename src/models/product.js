export default (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      allowNull: false,
    },
    name: DataTypes.STRING,
    minOrderPrice: {
      type: DataTypes.FLOAT,
    },
    description: DataTypes.TEXT,
    imageURL: {
      type: DataTypes.STRING,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
  }, {});

  Product.associate = (models) => {
    Product.belongsTo(models.Category, {
      foreignKey: {
        name: 'categoryId',
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    });
    Product.belongsToMany(models.Order, {
      through: models.OrderProduct,
      foreignKey: {
        name: 'productId',
      },
      otherKey: {
        name: 'orderId',
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    });
    Product.belongsTo(models.Restaurant, {
      foreignKey: {
        name: 'restaurantId',
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    });
  };

  return Product;
};
