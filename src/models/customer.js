import bcrypt from 'bcryptjs';
import { config } from 'dotenv';
import jwt from 'jsonwebtoken';

config();

export default (sequelize, DataTypes) => {
  const Customer = sequelize.define('Customer', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
    },
    firstName: {
      type: DataTypes.STRING,
    },
    lastName: {
      type: DataTypes.STRING,
    },
    avatar: {
      type: DataTypes.STRING,
    },
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    phone: DataTypes.STRING,
    isAdmin: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    status: {
      type: DataTypes.ENUM('active', 'suspended'),
      defaultValue: 'active',
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
  }, {});

  Customer.beforeCreate(async (customer) => {
    customer.password = await customer.generatePasswordHash();
  });

  Customer.beforeUpdate(async (customer) => {
    if (customer.changed('password')) {
      customer.password = await customer.generatePasswordHash();
    }
  });

  Customer.prototype.generatePasswordHash = async function generatePasswordHash() {
    const saltRounds = +process.env.SALT;
    return bcrypt.hash(this.password, saltRounds);
  };

  Customer.prototype.getSafeDataValues = function getSafeDataValues() {
    const { password, ...data } = this.dataValues;
    return data;
  };

  Customer.prototype.validatePassword = function validatePassword(password) {
    return bcrypt.compareSync(password, this.password);
  };

  Customer.prototype.generateVerificationToken = async function generateVerificationToken() {
    const secret = `${this.password}!${this.createdAt.toISOString()}`;
    return jwt.sign({ id: this.id }, secret, { expiresIn: '10m' });
  };

  Customer.prototype.decodeVerificationToken = async function decodeVerificationToken(token) {
    const secret = `${this.password}!${this.createdAt.toISOString()}`;
    return jwt.verify(token, secret);
  };

  Customer.getExistingUser = async (queryString, column = 'email') => {
    const customer = await Customer.findOne({
      where: { [column]: queryString },
    });
    return customer;
  };

  Customer.associate = (models) => {
    Customer.hasMany(models.Review, {
      foreignKey: {
        name: 'customerId',
      },
      onDelete: 'NO ACTION',
      onUpdate: 'CASCADE',
    });
    Customer.hasMany(models.Order, {
      foreignKey: {
        name: 'customerId',
      },
      onDelete: 'NO ACTION',
      onUpdate: 'CASCADE',
    });
    Customer.hasOne(models.Address, {
      foreignKey: {
        name: 'customerId',
      },
      onDelete: 'NO ACTION',
      onUpdate: 'CASCADE',
    });
  };

  return Customer;
};
