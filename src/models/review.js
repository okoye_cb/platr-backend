export default (sequelize, DataTypes) => {
  const Review = sequelize.define('Review', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      autoIncrement: false,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
    },
    starRating: {
      type: DataTypes.INTEGER,
    },
    summary: DataTypes.TEXT,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
  }, {});

  Review.associate = (models) => {
    Review.belongsTo(models.Customer, {
      foreignKey: { name: 'customerId' },
      onDelete: 'NO ACTION',
      onUpdate: 'CASCADE',
    });
    Review.belongsTo(models.Restaurant, {
      foreignKey: { name: 'restaurantId' },
      onDelete: 'NO ACTION',
      onUpdate: 'CASCADE',
    });
  };

  return Review;
};
