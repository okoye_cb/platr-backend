import bcrypt from 'bcryptjs';

export default (sequelize, DataTypes) => {
  const Restaurant = sequelize.define('Restaurant', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
    },
    about: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    businessName: {
      type: DataTypes.STRING,
    },
    phone: DataTypes.STRING,
    status: {
      type: DataTypes.ENUM('pending', 'rejected', 'active', 'inactive', 'suspended'),
      defaultValue: 'pending',
    },
    avatar: {
      type: DataTypes.STRING,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
  }, {});

  Restaurant.associate = (models) => {
    Restaurant.hasOne(models.Address, {
      foreignKey: { name: 'restaurantId' },
      onDelete: 'NO ACTION',
      onUpdate: 'CASCADE',
    });
    Restaurant.hasMany(models.Review, {
      foreignKey: { name: 'restaurantId' },
      onDelete: 'NO ACTION',
      onUpdate: 'CASCADE',
    });
    Restaurant.hasMany(models.Product, {
      foreignKey: { name: 'restaurantId' },
      onDelete: 'NO ACTION',
      onUpdate: 'CASCADE',
    });
    Restaurant.hasMany(models.Order, {
      foreignKey: { name: 'restaurantId' },
      onDelete: 'NO ACTION',
      onUpdate: 'CASCADE',
    });
  };

  Restaurant.beforeCreate(async (restaurant) => {
    restaurant.password = await restaurant.generatePasswordHash();
  });

  Restaurant.beforeUpdate(async (restaurant) => {
    if (restaurant.changed('password')) {
      restaurant.password = await restaurant.generatePasswordHash();
    }
  });

  Restaurant.prototype.toJSON = function() {
    const values = { ...this.get() };
    delete values.password;
    return values;
  };

  Restaurant.prototype.generatePasswordHash = async function generatePasswordHash() {
    const saltRounds = +process.env.SALT;
    return bcrypt.hash(this.password, saltRounds);
  };

  Restaurant.prototype.getSafeDataValues = function getSafeDataValues() {
    const { password, ...data } = this.dataValues;
    return data;
  };

  Restaurant.prototype.validatePassword = function validatePassword(password) {
    return bcrypt.compareSync(password, this.password);
  };

  Restaurant.getExistingRestaurant = async (queryString, column = 'email') => {
    const restaurant = await Restaurant.findOne({
      where: { [column]: queryString },
    });
    return restaurant;
  };

  return Restaurant;
};
