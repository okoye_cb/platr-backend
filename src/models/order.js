export default (sequelize, DataTypes) => {
  const Order = sequelize.define('Order', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      allowNull: false,
    },
    deliveryFee: {
      type: DataTypes.FLOAT,
    },
    totalPrice: {
      type: DataTypes.FLOAT,
    },
    totalQuantity: {
      type: DataTypes.INTEGER,
    },
    paymentMethod: {
      type: DataTypes.STRING,
      defaultValue: 'card',
    },
    status: {
      type: DataTypes.ENUM('cancelled', 'in-progress', 'ready', 'delivered'),
    },
    paymentStatus: {
      type: DataTypes.ENUM('successful', 'failed'),
    },
    customerName: {
      type: DataTypes.STRING,
    },
    customerEmail: {
      type: DataTypes.STRING,
    },
    customerPhone: {
      type: DataTypes.STRING,
    },
    deliveryAddress: {
      type: DataTypes.STRING,
    },
    txnReference: {
      type: DataTypes.STRING,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
  }, {});

  Order.associate = (models) => {
    Order.belongsTo(models.Customer, {
      foreignKey: {
        name: 'customerId',
      },
      onDelete: 'NO ACTION',
      onUpdate: 'CASCADE',
    });
    Order.belongsTo(models.Restaurant, {
      foreignKey: {
        name: 'restaurantId',
      },
      as: 'restaurant',
      onDelete: 'NO ACTION',
      onUpdate: 'CASCADE',
    });
    Order.belongsToMany(models.Product, {
      through: models.OrderProduct,
      foreignKey: {
        name: 'orderId',
      },
      otherKey: {
        name: 'productId',
      },
      onDelete: 'NO ACTION',
      onUpdate: 'CASCADE',
    });
  };

  return Order;
};
